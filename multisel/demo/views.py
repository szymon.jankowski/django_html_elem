from django.shortcuts import render, redirect, get_object_or_404
import requests

# Create your views here.

def index(request):
    response = requests.get('https://my.api.mockaroo.com/people.json?key=67040650')
    people_response = response.json()
    return render(request, 'index.html', {
        'people_list': people_response
    })

