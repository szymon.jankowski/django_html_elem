from django import template

register = template.Library()

@register.inclusion_tag('includes/custom_select.html')
def custom_select(people):
    return {'entry_set': people}
